package br.com.controleacesso.cliente.controller;

import br.com.controleacesso.cliente.controller.request.ClienteRequest;
import br.com.controleacesso.cliente.controller.response.ClienteResponse;
import br.com.controleacesso.cliente.dataprovider.model.Cliente;
import br.com.controleacesso.cliente.mapper.ClienteMapper;
import br.com.controleacesso.cliente.service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/cliente")
public class ClienteController {

    @Autowired
    private ClienteService clienteService;

    @Autowired
    private ClienteMapper clienteMapper;

    /**
     * Cadastrar cliente
     * @param clienteRequest
     * @return
     */
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ClienteResponse cadastrarCliente(@Valid @RequestBody ClienteRequest clienteRequest) {
        Cliente cliente = clienteService.cadastrarCliente(clienteMapper.converterParaCliente(clienteRequest));
        return clienteMapper.converterParaCadastroClienteResponse(cliente);
    }

    /**
     * Consultar cliente por id
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public ClienteResponse consultarClientePorId(@PathVariable(name = "id") long id) {
        Cliente cliente = clienteService.consultarClientePorId(id);
        return clienteMapper.converterParaCadastroClienteResponse(cliente);
    }
}
