package br.com.controleacesso.cliente.exception;

public class GenericException extends RuntimeException {

    public GenericException(String message, Throwable cause) {
        super(message, cause);
    }
}
