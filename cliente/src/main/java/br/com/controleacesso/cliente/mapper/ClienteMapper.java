package br.com.controleacesso.cliente.mapper;

import br.com.controleacesso.cliente.controller.request.ClienteRequest;
import br.com.controleacesso.cliente.controller.response.ClienteResponse;
import br.com.controleacesso.cliente.dataprovider.model.Cliente;
import org.springframework.stereotype.Component;

@Component
public class ClienteMapper {

    /**
     * Converter um Cliente para um Cliente Response
     * @param cliente
     * @return
     */
    public ClienteResponse converterParaCadastroClienteResponse(Cliente cliente) {
        ClienteResponse clienteResponse = new ClienteResponse();
        clienteResponse.setId(cliente.getId());
        clienteResponse.setNome(cliente.getNome());
        return clienteResponse;
    }

    /**
     * Converte um cadastroClienteRequest em um Cliente
     * @param clienteRequest
     * @return
     */
    public Cliente converterParaCliente(ClienteRequest clienteRequest) {
        Cliente cliente = new Cliente();
        cliente.setNome(clienteRequest.getNome());
        return cliente;
    }
}
