package br.com.controleacesso.cliente.dataprovider.repository;

import br.com.controleacesso.cliente.dataprovider.model.Cliente;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClienteRepository extends JpaRepository<Cliente, Long> {
}
