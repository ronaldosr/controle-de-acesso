package br.com.controleacesso.cliente.service;

import br.com.controleacesso.cliente.dataprovider.model.Cliente;
import br.com.controleacesso.cliente.dataprovider.repository.ClienteRepository;
import br.com.controleacesso.cliente.exception.GenericException;
import br.com.controleacesso.cliente.exception.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
public class ClienteService {

    @Autowired
    private ClienteRepository clienteRepository;

    /**
     * Cadastrar cliente
     * @param cliente
     * @return
     */
    @Transactional
    public Cliente cadastrarCliente(Cliente cliente) {
        try {
            return clienteRepository.save(cliente);
        } catch (DataAccessException e) {
            throw new RuntimeException("Erro ao cadastro o cliente", e);
        } catch (RuntimeException e) {
            throw new GenericException("Erro no serviço de cliente", e);
        }
    }

    /**
     * Consultar cliente por id
     * @param id
     * @return
     */
    public Cliente consultarClientePorId(long id) {
        try {
            Optional<Cliente> cliente = clienteRepository.findById(id);
            if (!cliente.isPresent()) {
                throw new ResourceNotFoundException("Cliente não encontrado com o id " + id);
            }
            return cliente.get();
        } catch (DataAccessException e) {
            throw new GenericException("Erro no serviço de cliente", e);
        }
    }
}
