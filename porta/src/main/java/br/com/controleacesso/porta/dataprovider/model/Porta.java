package br.com.controleacesso.porta.dataprovider.model;

import javax.persistence.*;

@Entity
@Table(name = "porta")
public class Porta {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "andar", nullable = false, length = 2)
    private String andar;

    @Column(name = "sala", nullable = false, length = 5)
    private String sala;

    public Porta() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getAndar() {
        return andar;
    }

    public void setAndar(String andar) {
        this.andar = andar;
    }

    public String getSala() {
        return sala;
    }

    public void setSala(String sala) {
        this.sala = sala;
    }
}
