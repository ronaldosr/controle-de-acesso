package br.com.controleacesso.porta.controller.request;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class PortaRequest {

    @NotEmpty(message = "Informe o andar")
    @Size(min = 1, max = 2, message = "É possível cadastrar do 1º até o 99º andar")
    private String andar;

    @NotEmpty(message = "Informe a sala")
    @Size(min = 5, max = 5, message = "O número da sala deve conter 5 dígitos")
    private String sala;

    public PortaRequest() {
    }

    public String getAndar() {
        return andar;
    }

    public void setAndar(String andar) {
        this.andar = andar;
    }

    public String getSala() {
        return sala;
    }

    public void setSala(String sala) {
        this.sala = sala;
    }
}
