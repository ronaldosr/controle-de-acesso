package br.com.controleacesso.porta.controller;

import br.com.controleacesso.porta.controller.request.PortaRequest;
import br.com.controleacesso.porta.controller.response.PortaResponse;
import br.com.controleacesso.porta.dataprovider.model.Porta;
import br.com.controleacesso.porta.mapper.PortaMapper;
import br.com.controleacesso.porta.service.PortaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/porta")
public class PortaController {

    @Autowired
    private PortaService portaService;

    @Autowired
    private PortaMapper portaMapper;

    /**
     * Cadastrar porta
     * @param portaRequest
     * @return
     */
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public PortaResponse cadastrarPorta(@Valid @RequestBody PortaRequest portaRequest) {
        Porta porta = portaService.cadastrarPorta(portaMapper.converterParaPorta(portaRequest));
        return portaMapper.converterParaPortaResponse(porta);
    }

    /**
     * Consultar porta por id
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public PortaResponse consultarPortaPorId(@PathVariable(name = "id") long id) {
        Porta porta = portaService.consultarPortaPorId(id);
        return portaMapper.converterParaPortaResponse(porta);
    }
}
