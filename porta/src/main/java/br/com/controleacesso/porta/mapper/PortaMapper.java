package br.com.controleacesso.porta.mapper;

import br.com.controleacesso.porta.controller.request.PortaRequest;
import br.com.controleacesso.porta.controller.response.PortaResponse;
import br.com.controleacesso.porta.dataprovider.model.Porta;
import org.springframework.stereotype.Component;

@Component
public class PortaMapper {
    public Porta converterParaPorta(PortaRequest portaRequest) {
        Porta porta = new Porta();
        porta.setAndar(portaRequest.getAndar());
        porta.setSala(portaRequest.getSala());
        return porta;
    }

    public PortaResponse converterParaPortaResponse(Porta porta) {
        PortaResponse portaResponse = new PortaResponse();
        portaResponse.setId(porta.getId());
        portaResponse.setAndar(porta.getAndar());
        portaResponse.setSala(porta.getSala());
        return portaResponse;
    }
}
