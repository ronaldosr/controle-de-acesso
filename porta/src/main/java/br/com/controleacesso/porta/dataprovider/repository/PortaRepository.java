package br.com.controleacesso.porta.dataprovider.repository;

import br.com.controleacesso.porta.dataprovider.model.Porta;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PortaRepository extends JpaRepository<Porta, Long> {
}
