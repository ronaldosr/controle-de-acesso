package br.com.controleacesso.porta.exception;

public class GenericException extends RuntimeException {

    public GenericException(String message, Throwable cause) {
        super(message, cause);
    }
}
