package br.com.controleacesso.porta.service;

import br.com.controleacesso.porta.dataprovider.model.Porta;
import br.com.controleacesso.porta.dataprovider.repository.PortaRepository;
import br.com.controleacesso.porta.exception.GenericException;
import br.com.controleacesso.porta.exception.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PortaService {

    @Autowired
    private PortaRepository portaRepository;

    /**
     * Cadastrar porta
     * @param porta
     * @return
     */
    public Porta cadastrarPorta(Porta porta) {
        try {
            return portaRepository.save(porta);
        } catch (DataAccessException e) {
            throw new RuntimeException("Erro ao cadastrar a porta" , e);
        } catch (RuntimeException e) {
            throw new GenericException("Erro no serviço de porta", e);
        }
    }

    /**
     * Consultar porta por id
     * @param id
     * @return
     */
    public Porta consultarPortaPorId(long id) {
        try {
            Optional<Porta> porta = portaRepository.findById(id);
            if (!porta.isPresent()) {
                throw new ResourceNotFoundException("Porta não encontrada com o id " + id);
            }
            return porta.get();
        } catch (DataAccessException e) {
            throw new GenericException("Erro no serviço de porta", e);
        }
    }
}
