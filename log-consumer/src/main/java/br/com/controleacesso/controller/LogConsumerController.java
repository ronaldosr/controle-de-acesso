package br.com.controleacesso.controller;

import br.com.controleacesso.acesso.controller.response.LogAcessoResponse;
import br.com.controleacesso.utilitario.LogAcessoCsv;
import com.opencsv.*;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.enums.CSVReaderNullFieldIndicator;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

@Component
public class LogConsumerController {

    @Autowired
    LogAcessoCsv logAcessoCsv;

    @KafkaListener(topics = "spec4-ronaldo-soares-1", groupId = "nogroup")
    public void receberLog(@Payload LogAcessoResponse logAcessoResponse) throws CsvException {

        try {
            logAcessoCsv.printLogSysout(logAcessoResponse);
            logAcessoCsv.gravarLogAcessoCsv(logAcessoResponse);

        } catch (IOException e) {
            throw new RuntimeException("Erro de entrada/saída: " + e , e);
        } catch (CsvDataTypeMismatchException e) {
            throw new RuntimeException("Erro na conversão de dados do csv: " + e, e);
        } catch (CsvRequiredFieldEmptyException e) {
            throw new RuntimeException("Erro na geração do csv: " + e, e);
        }
    }


}
