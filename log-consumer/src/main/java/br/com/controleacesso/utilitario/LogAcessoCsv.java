package br.com.controleacesso.utilitario;

import br.com.controleacesso.acesso.controller.response.LogAcessoResponse;
import com.opencsv.*;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.enums.CSVReaderNullFieldIndicator;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

@Component
public class LogAcessoCsv {

    public void gravarLogAcessoCsv(LogAcessoResponse logAcessoResponse) throws IOException, CsvException {

        String arquivo = "Log Acesso.csv";
        File file = new File(arquivo);

        if (file.exists()) {
            adicionarNovaEntradaAoLog(arquivo, logAcessoResponse);
        } else {
            criarNovoLog(arquivo, logAcessoResponse);
        }
    }

    private void adicionarNovaEntradaAoLog(String arquivo, LogAcessoResponse logAcessoResponse) throws IOException, CsvException {
        List<LogAcessoResponse> logCompleto = lerArquivoExistente(arquivo);
        complementarLog(arquivo, logCompleto, logAcessoResponse);
    }

    private void complementarLog(String arquivo, List<LogAcessoResponse> logCompleto, LogAcessoResponse logAcessoResponse) throws IOException, CsvDataTypeMismatchException, CsvRequiredFieldEmptyException {
        Writer writer = Files.newBufferedWriter(Paths.get(arquivo));
        StatefulBeanToCsv<LogAcessoResponse> beanToCsv =  new StatefulBeanToCsvBuilder<LogAcessoResponse>(writer)
                .withSeparator(CSVWriter.NO_QUOTE_CHARACTER)
                .withSeparator(';')
                .withOrderedResults(true)
                .build();

        beanToCsv.write(logAcessoResponse);
        beanToCsv.write(logCompleto);

        writer.flush();
        writer.close();
    }

    private List<LogAcessoResponse> lerArquivoExistente(String arquivo) throws IOException, CsvException {
        Reader reader = Files.newBufferedReader(Paths.get(arquivo));

        CSVParser parser = new CSVParserBuilder()
                .withSeparator(';')
                .withFieldAsNull(CSVReaderNullFieldIndicator.EMPTY_QUOTES)
                .withIgnoreLeadingWhiteSpace(true)
                .withIgnoreQuotations(true)
                .build();

        CSVReader csvReader = new CSVReaderBuilder(reader)
                .withSkipLines(1)
                .withCSVParser(parser)
                .build();

        List<String[]> logCompleto = csvReader.readAll();
        List<LogAcessoResponse> listaLogAcessoResponse = new ArrayList<>();
        for (String[] record : logCompleto) {
            LogAcessoResponse itemLog = new LogAcessoResponse();
            itemLog.setAcesso(Boolean.parseBoolean(record[0]));
            itemLog.setCliente_id(Long.parseLong(record[1]));
            itemLog.setPorta_id(Long.parseLong(record[2]));
            listaLogAcessoResponse.add(itemLog);
        }

        csvReader.close();
        return  listaLogAcessoResponse;
    }

    private void criarNovoLog(String arquivo, LogAcessoResponse logAcessoResponse) throws IOException, CsvDataTypeMismatchException, CsvRequiredFieldEmptyException {
        Writer writer = Files.newBufferedWriter(Paths.get(arquivo));
        StatefulBeanToCsv<LogAcessoResponse> beanToCsv =  new StatefulBeanToCsvBuilder<LogAcessoResponse>(writer)
                .withSeparator(CSVWriter.NO_QUOTE_CHARACTER)
                .withSeparator(';')
                .withOrderedResults(true)
                .build();

        beanToCsv.write(logAcessoResponse);;
        writer.flush();
        writer.close();
    }

    public void printLogSysout(LogAcessoResponse logAcessoResponse) {

        String situacao = logAcessoResponse.isAcesso() ? "Ativo" : "Inativo";
        System.out.println("Mensagem ===> " +
                "ClienteId:" + logAcessoResponse.getCliente_id() + " " +
                "PortaId: " + logAcessoResponse.getPorta_id() + " " +
                "Situação: " + situacao);
    }
}
