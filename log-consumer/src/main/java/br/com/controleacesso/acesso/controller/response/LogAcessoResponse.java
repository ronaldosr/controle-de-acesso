package br.com.controleacesso.acesso.controller.response;

import com.opencsv.bean.CsvBindByName;

public class LogAcessoResponse {

    @CsvBindByName
    private long porta_id;

    @CsvBindByName
    private long cliente_id;

    @CsvBindByName
    private boolean acesso;

    public LogAcessoResponse() {
    }

    public long getPorta_id() {
        return porta_id;
    }

    public void setPorta_id(long porta_id) {
        this.porta_id = porta_id;
    }

    public long getCliente_id() {
        return cliente_id;
    }

    public void setCliente_id(long cliente_id) {
        this.cliente_id = cliente_id;
    }

    public boolean isAcesso() {
        return acesso;
    }

    public void setAcesso(boolean acesso) {
        this.acesso = acesso;
    }

    @Override
    public String toString() {
        return "LogAcessoResponse{" +
                "porta_id=" + porta_id +
                ", cliente_id=" + cliente_id +
                ", acesso=" + acesso +
                '}';
    }
}
