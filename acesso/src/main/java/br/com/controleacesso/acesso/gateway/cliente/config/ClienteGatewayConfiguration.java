package br.com.controleacesso.acesso.gateway.cliente.config;

import br.com.controleacesso.acesso.gateway.cliente.exception.ClienteDecoder;
import feign.codec.ErrorDecoder;
import org.springframework.context.annotation.Bean;

public class ClienteGatewayConfiguration {

    @Bean
    public ErrorDecoder getErrorDecoder() {
        return new ClienteDecoder();
    }
}
