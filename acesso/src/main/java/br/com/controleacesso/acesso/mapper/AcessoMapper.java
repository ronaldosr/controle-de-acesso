package br.com.controleacesso.acesso.mapper;

import br.com.controleacesso.acesso.controller.request.AcessoRequest;
import br.com.controleacesso.acesso.controller.response.AcessoResponse;
import br.com.controleacesso.acesso.dataprovider.model.Acesso;
import org.springframework.stereotype.Component;

@Component
public class AcessoMapper {
    public Acesso converterParaAcesso(AcessoRequest acessoRequest) {
        Acesso acesso = new Acesso();
        acesso.setClienteId(acessoRequest.getCliente_id());
        acesso.setPortaId(acessoRequest.getPorta_id());
        return acesso;
    }

    public AcessoResponse converterParaAcessoResponse(Acesso acesso) {
        AcessoResponse acessoResponse = new AcessoResponse();
        acessoResponse.setCliente_Id(acesso.getClienteId());
        acessoResponse.setPorta_id(acesso.getPortaId());
        return acessoResponse;
    }
}
