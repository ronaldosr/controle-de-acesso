package br.com.controleacesso.acesso.service;

import br.com.controleacesso.acesso.controller.response.LogAcessoResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class AcessoProducer {

    @Autowired
    private KafkaTemplate<String, LogAcessoResponse> acessoProducer;

    public void registrarLogKafka(LogAcessoResponse logAcessoResponse) {
        acessoProducer.send("spec4-ronaldo-soares-1", logAcessoResponse);
    }
}
