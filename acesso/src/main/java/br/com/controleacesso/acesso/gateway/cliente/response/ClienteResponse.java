package br.com.controleacesso.acesso.gateway.cliente.response;

public class ClienteResponse {

    private long id;
    private String nome;

    public ClienteResponse() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
