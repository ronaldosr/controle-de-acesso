package br.com.controleacesso.acesso.gateway.porta.config;

import br.com.controleacesso.acesso.gateway.porta.exception.PortaDecoder;
import feign.codec.ErrorDecoder;
import org.springframework.context.annotation.Bean;

public class PortaGatewayConfiguration {

    @Bean
    public ErrorDecoder getErrorDecoder() {
        return new PortaDecoder();
    }
}
