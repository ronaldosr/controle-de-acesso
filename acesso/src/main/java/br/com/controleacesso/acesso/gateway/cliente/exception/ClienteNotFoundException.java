package br.com.controleacesso.acesso.gateway.cliente.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Cliente inválido")
public class ClienteNotFoundException extends RuntimeException {

    public ClienteNotFoundException(Throwable cause) {
        super(cause);
    }

    public ClienteNotFoundException() {
    }
}
