package br.com.controleacesso.acesso.dataprovider.model;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "acesso")
public class Acesso {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "clienteId", nullable = false)
    private long  clienteId;

    @Column(name = "portaId", nullable = false)
    private long portaId;

    @Column(name = "dataHoraCadastro", nullable = false)
    private LocalDate dataHoraCadastro;

    public Acesso() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getClienteId() {
        return clienteId;
    }

    public void setClienteId(long clienteId) {
        this.clienteId = clienteId;
    }

    public long getPortaId() {
        return portaId;
    }

    public void setPortaId(long portaId) {
        this.portaId = portaId;
    }

    public LocalDate getDataHoraCadastro() {
        return dataHoraCadastro;
    }

    public void setDataHoraCadastro(LocalDate dataHoraCadastro) {
        this.dataHoraCadastro = dataHoraCadastro;
    }
}
