package br.com.controleacesso.acesso.gateway;

import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RoundRobinRule;
import org.springframework.context.annotation.Bean;

public class RibbonConfiguration {

    @Bean
    public IRule obterRule() {
        return new RoundRobinRule();
    }

    //RoundRobin: uma vez de cada serviço
    //WeightedResponseTimeRule: a prioridade é do serviço com menor ping
    //RandomRule: escolha dos serviços é aleatória
}
