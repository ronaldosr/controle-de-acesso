package br.com.controleacesso.acesso.dataprovider.repository;

import br.com.controleacesso.acesso.dataprovider.model.Acesso;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AcessoRepository extends JpaRepository<Acesso, Long> {

    Optional<Acesso> findByClienteIdAndPortaId(long clienteId, long portaId);

    void deleteByClienteIdAndPortaId(long clienteId, long portaId);
}
