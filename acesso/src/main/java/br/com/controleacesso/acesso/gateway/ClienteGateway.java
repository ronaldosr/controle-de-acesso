package br.com.controleacesso.acesso.gateway;

import br.com.controleacesso.acesso.gateway.cliente.config.ClienteGatewayConfiguration;
import br.com.controleacesso.acesso.gateway.cliente.response.ClienteResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "CLIENTE", configuration = ClienteGatewayConfiguration.class)
public interface ClienteGateway {

    @GetMapping("/cliente/{id}")
    ClienteResponse consultarClientePorId(@PathVariable(name = "id") long id);
}
