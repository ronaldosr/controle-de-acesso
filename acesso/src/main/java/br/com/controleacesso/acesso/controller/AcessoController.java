package br.com.controleacesso.acesso.controller;

import br.com.controleacesso.acesso.controller.request.AcessoRequest;
import br.com.controleacesso.acesso.controller.response.AcessoResponse;
import br.com.controleacesso.acesso.dataprovider.model.Acesso;
import br.com.controleacesso.acesso.mapper.AcessoMapper;
import br.com.controleacesso.acesso.service.AcessoProducer;
import br.com.controleacesso.acesso.service.AcessoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/acesso")
public class AcessoController {

    @Autowired
    private AcessoService acessoService;

    @Autowired
    private AcessoMapper acessoMapper;

    @Autowired
    private AcessoProducer acessoProducer;

    /**
     * Cadastrar acesso com porta id e cliente id
     * @param acessoRequest
     * @return
     */
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public AcessoResponse cadastrarAcesso(@Valid @RequestBody AcessoRequest acessoRequest) {
        Acesso acesso = acessoService.cadastrarAcesso(acessoMapper.converterParaAcesso(acessoRequest));
        return acessoMapper.converterParaAcessoResponse(acesso);
    }

    /**
     * Consultar acesso com porta id e cliente id
     * @param portaId
     * @param clienteId
     * @return
     */
    @GetMapping("/{cliente_id}/{porta_id}")
    public AcessoResponse consultarAcesso(@PathVariable(name = "cliente_id") long clienteId,
                                          @PathVariable(name = "porta_id") long portaId) {

        Acesso acesso = acessoService.consultarAcesso(portaId, clienteId);
        return acessoMapper.converterParaAcessoResponse(acesso);
    }

    /**
     * Excluir acesso com porta id e cliente id
     * @param portaId
     * @param clienteId
     */
    @DeleteMapping("/{cliente_id}/{porta_id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void excluirAcesso(@PathVariable(name = "cliente_id") long clienteId,
                              @PathVariable(name = "porta_id") long portaId) {
        acessoService.excluirAcesso(portaId, clienteId);
    }
}
