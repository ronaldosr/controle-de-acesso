package br.com.controleacesso.acesso.controller.response;

import javax.validation.constraints.NotNull;

public class AcessoResponse {

    private long porta_id;
    private long cliente_Id;

    public AcessoResponse() {
    }

    public long getPorta_id() {
        return porta_id;
    }

    public void setPorta_id(long porta_id) {
        this.porta_id = porta_id;
    }

    public long getCliente_Id() {
        return cliente_Id;
    }

    public void setCliente_Id(long cliente_Id) {
        this.cliente_Id = cliente_Id;
    }
}
