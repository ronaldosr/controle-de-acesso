package br.com.controleacesso.acesso.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class AcessoJaExisteException extends RuntimeException{

    public AcessoJaExisteException(String message) {
        super(message);
    }

}
