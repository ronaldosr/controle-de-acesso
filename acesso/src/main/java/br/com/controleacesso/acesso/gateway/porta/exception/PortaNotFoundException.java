package br.com.controleacesso.acesso.gateway.porta.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Porta inválida")
public class PortaNotFoundException extends RuntimeException {

    public PortaNotFoundException(Throwable cause) {
        super(cause);
    }

    public PortaNotFoundException() {
    }
}
