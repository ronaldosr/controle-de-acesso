package br.com.controleacesso.acesso.service;

import br.com.controleacesso.acesso.controller.response.LogAcessoResponse;
import br.com.controleacesso.acesso.dataprovider.model.Acesso;
import br.com.controleacesso.acesso.dataprovider.repository.AcessoRepository;
import br.com.controleacesso.acesso.exception.AcessoJaExisteException;
import br.com.controleacesso.acesso.exception.ResourceNotFoundException;
import br.com.controleacesso.acesso.gateway.ClienteGateway;
import br.com.controleacesso.acesso.gateway.cliente.exception.ClienteNotFoundException;
import br.com.controleacesso.acesso.gateway.cliente.response.ClienteResponse;
import br.com.controleacesso.acesso.gateway.PortaGateway;
import br.com.controleacesso.acesso.gateway.porta.exception.PortaNotFoundException;
import br.com.controleacesso.acesso.gateway.porta.response.PortaResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.Optional;

@Service
public class AcessoService {

    @Autowired
    private AcessoRepository acessoRepository;

    @Autowired
    private ClienteGateway clienteGateway;

    @Autowired
    private PortaGateway portaGateway;

    @Autowired
    private AcessoProducer acessoProducer;

    /**
     * Cadastrar acesso de um cliente a uma porta
     * @param acesso
     * @return
     */
    public Acesso cadastrarAcesso(Acesso acesso) {
        try {
            ClienteResponse clienteResponse = clienteGateway.consultarClientePorId(acesso.getClienteId());
            PortaResponse portaResponse = portaGateway.consultarPortaPorId(acesso.getPortaId());

            Acesso acessoAtual = consultarAcesso(acesso.getPortaId(), acesso.getClienteId());
            if (acessoAtual.getClienteId() == acesso.getClienteId() &&
                    acessoAtual.getPortaId() == acesso.getPortaId()) {
                throw new AcessoJaExisteException("Cliente já possui acesso à porta");
            } else {
                acesso.setDataHoraCadastro(LocalDate.now());
                return acessoRepository.save(acesso);
            }
        } catch (ClienteNotFoundException e) {
            throw new ClienteNotFoundException(e);
        } catch (PortaNotFoundException e) {
            throw new PortaNotFoundException(e);
        } catch (DataAccessException e) {
            throw new RuntimeException("Erro no cadastro do acesso: " + e, e);
        }
    }

    /**
     * Consultar acesso com porta id e cliente id
     * @param portaId
     * @param clienteId
     * @return
     */
    public Acesso consultarAcesso(long portaId, long clienteId) {
        try {
            Optional<Acesso> acesso = acessoRepository.findByClienteIdAndPortaId(clienteId, portaId);

            LogAcessoResponse logAcessoResponse = new LogAcessoResponse();
            logAcessoResponse.setCliente_id(clienteId);
            logAcessoResponse.setPorta_id(portaId);

            if (!acesso.isPresent()) {
                logAcessoResponse.setAcesso(false);
                acessoProducer.registrarLogKafka(logAcessoResponse);
                throw new ResourceNotFoundException("Cliente não possui acesso à porta");
            }

            logAcessoResponse.setAcesso(true);
            acessoProducer.registrarLogKafka(logAcessoResponse);

            return acesso.get();
        } catch (ClienteNotFoundException e) {
            throw new ClienteNotFoundException(e);
        } catch (PortaNotFoundException e) {
            throw new PortaNotFoundException(e);
        } catch (DataAccessException e) {
            throw new RuntimeException("Erro ao consultar o acesso: " + e, e);
        }
    }

    /**
     * Excluir acesso com porta id e cliente id
     * @param portaId
     * @param clienteId
     */
    @Transactional
    public void excluirAcesso(long portaId, long clienteId) {
        try {
//            Acesso acesso = consultarAcesso(portaId, clienteId);
//            acessoRepository.deleteById(acesso.getId());
            acessoRepository.deleteByClienteIdAndPortaId(clienteId, portaId);
        } catch (DataAccessException e) {
            throw new RuntimeException("Erro ao excluir o acesso: " + e, e);
        }
    }
}
