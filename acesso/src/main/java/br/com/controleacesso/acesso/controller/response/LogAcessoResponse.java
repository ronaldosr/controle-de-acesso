package br.com.controleacesso.acesso.controller.response;

public class LogAcessoResponse {

    private long porta_id;
    private long cliente_id;
    private boolean acesso;

    public LogAcessoResponse() {
    }

    public long getPorta_id() {
        return porta_id;
    }

    public void setPorta_id(long porta_id) {
        this.porta_id = porta_id;
    }

    public long getCliente_id() {
        return cliente_id;
    }

    public void setCliente_id(long cliente_id) {
        this.cliente_id = cliente_id;
    }

    public boolean isAcesso() {
        return acesso;
    }

    public void setAcesso(boolean acesso) {
        this.acesso = acesso;
    }
}
