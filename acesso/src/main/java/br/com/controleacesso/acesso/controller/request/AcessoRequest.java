package br.com.controleacesso.acesso.controller.request;

import javax.validation.constraints.NotNull;

public class AcessoRequest {

    @NotNull(message = "Informe o id da porta")
    private long porta_id;

    @NotNull(message = "Informe o id do cliente")
    private long cliente_id;

    public AcessoRequest() {
    }

    public long getPorta_id() {
        return porta_id;
    }

    public void setPorta_id(long porta_id) {
        this.porta_id = porta_id;
    }

    public long getCliente_id() {
        return cliente_id;
    }

    public void setCliente_id(long cliente_id) {
        this.cliente_id = cliente_id;
    }
}
