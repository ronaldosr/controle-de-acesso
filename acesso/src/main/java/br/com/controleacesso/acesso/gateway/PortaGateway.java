package br.com.controleacesso.acesso.gateway;

import br.com.controleacesso.acesso.gateway.porta.config.PortaGatewayConfiguration;
import br.com.controleacesso.acesso.gateway.porta.response.PortaResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "PORTA", configuration = PortaGatewayConfiguration.class)
public interface PortaGateway {

    @GetMapping("/porta/{id}")
    PortaResponse consultarPortaPorId(@PathVariable(name = "id") long id);
}
